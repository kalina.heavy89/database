//***********************************************************************
//Creating tables

CREATE TABLE `Categories` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = latin1;

CREATE TABLE `Products` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`price` decimal(10,2) NOT NULL,
	`quantity` int(11) DEFAULT NULL,
	`category_id` int(11) NOT NULL,
	PRIMARY KEY (`id`),
	KEY (`category_id`),
	CONSTRAINT `products_catid` FOREIGN KEY (`category_id`) REFERENCES `Categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET=latin1;

CREATE TABLE `Users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL,
	`password` varchar(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `Carts` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) DEFAULT NULL,
	`total_price` decimal(10,2) NOT NULL,
	`order_date` date DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `user_id` (`user_id`),
	CONSTRAINT `carts_uid` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`)
)ENGINE=InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET=latin1;

CREATE TABLE `Cart_items` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`cart_id` int(11) DEFAULT NULL,
	`product_id` int(11) DEFAULT NULL,
	`quantity` int(11) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `cart_id` (`cart_id`),
	KEY `product_id` (`product_id`),
	CONSTRAINT `cart_items_cid` FOREIGN KEY (`cart_id`) REFERENCES `Carts` (`id`),
	CONSTRAINT `cart_items_pid` FOREIGN KEY (`product_id`) REFERENCES `Products` (`id`)
)ENGINE=InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET=latin1;

//***********************************************************************
//Filling tables

//Filling table Categories
INSERT INTO `Categories` (`name`) VALUES ('food');
INSERT INTO `Categories` (`name`) VALUES ('clothes');
INSERT INTO `Categories` (`name`) VALUES ('shoes');
INSERT INTO `Categories` (`name`) VALUES ('dishes'); 

//Filling table Products:
//Category food:
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Breed', '12.50', '10', '1');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Milk', '19.75', '8', '1');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Fish', '74.90', '25', '1');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Meat', '81.20', '48', '1');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Zucchini', '31.50', '18', '1');
//Category clothes:
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Trousers', '450.25', '5', '2');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Shorts', '240.50', '8', '2');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('T-shirts', '380.75', '11', '2');
//Category shoes: 
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Sandals', '470.35', '5', '3');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Shoes', '880.20', '9', '3');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Boots', '1020.50', '8', '3');
//Category dishes:
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Spoons', '80.15', '25', '4');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Forks', '82.50', '30', '4');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Plates', '120.40', '20', '4');
INSERT INTO `Products` (`name`, `price`, `quantity`, `category_id`) 
VALUES ('Pans', '650.35', '8', '4');

//Filling table Users:
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('Igor_ABC', 'igor_abc@mail.com', 'd45d12bd2');
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('HannaIrs', 'hannaIrs@mail.com', 'D4hDf5h42');
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('EgorBRs', 'egor@mail.com', '55374DDGAHJ');
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('IrenAdler', 'iren@mail.com', 'IrnAdlr');
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('GregHouse', 'greg@mail.com', 'DoctorHouse');
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('RoseTyler', 'rose@mail.com', 'Rosa0509085');
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('OstapBender', 'ostap@mail.com', 'Osia777');
INSERT INTO `Users` (`name`, `email`, `password`) 
VALUES ('AndyJames', 'andy@mail.com', 'AndyJ140585');

//Filling table Carts:
INSERT INTO `Carts` (`user_id`, `total_price`, `order_date`) 
VALUES ('3', '495.15', '2020-13-11');
INSERT INTO `Carts` (`user_id`, `total_price`, `order_date`) 
VALUES ('7', '905.25', '2020-30-11');

//Filling table Cart_items:
//First cart:
INSERT INTO `Cart_items` (`cart_id`, `product_id`, `quantity`) 
VALUES ('1', '2', '2');
INSERT INTO `Cart_items` (`cart_id`, `product_id`, `quantity`) 
VALUES ('1', '3', '1');
INSERT INTO `Cart_items` (`cart_id`, `product_id`, `quantity`) 
VALUES ('1', '8', '1');
//Second cart:
INSERT INTO `Cart_items` (`cart_id`, `product_id`, `quantity`) 
VALUES ('2', '1', '2');
INSERT INTO `Cart_items` (`cart_id`, `product_id`, `quantity`) 
VALUES ('2', '4', '2');
INSERT INTO `Cart_items` (`cart_id`, `product_id`, `quantity`) 
VALUES ('2', '9', '1');
INSERT INTO `Cart_items` (`cart_id`, `product_id`, `quantity`) 
VALUES ('2', '13', '3');

//**********************************************************************
//Querries:

//Total quantities of all products:
SELECT SUM(`quantity`) 
FROM `Products`;
//Result:
240

//Product with the max number of units
SELECT `name`, MAX(`quantity`)
FROM `Products` 
GROUP BY `name` 
ORDER BY MAX(`quantity`) DESC
LIMIT 1;
//Result:
name	MAX(`quantity`)	
Meat	49

//Increasing product with minimum number of units in 2 times
UPDATE `Products` 
SET `quantity` = `quantity` * 2 
WHERE
	`quantity` = ( SELECT MIN( `quantity` ) 
				   FROM ( SELECT `quantity` 
						  FROM `Products` 
						  GROUP BY `quantity` 
						) AS min_q 
				  );

//Showing data from all users with products in the cart and their products 
	//that added into the cart
SELECT `Users`.`name` as `user_name`, `Users`.`email`, `Users`.`password`, 
	`Products`.`name` as `product_name`, `Products`.`price`, 
	`Cart_items`.`quantity`, `Carts`.`total_price`, `Carts`.`order_date`
FROM ((`Carts` 
INNER JOIN `Users` 
ON `Carts`.`user_id` = `Users`.`id`)
INNER JOIN `Cart_items`
ON `Carts`.`id` = `Cart_items`.`cart_id`)
INNER JOIN `Products`
ON `Cart_items`.`product_id` = `Products`.`id`;

//Result:
name	email				password	product_name	price	quantity	total_price	order_date
EgorBRs	egor@mail.com		55374DDGAHJ	Milk			19,75	2			495,15		2020-11-13
EgorBRs	egor@mail.com		55374DDGAHJ	Fish			74,90	1			495,15		2020-11-13
EgorBRs	egor@mail.com		55374DDGAHJ	T-shirts		380,75	1			495,15		2020-11-13
OstapBender	ostap@mail.com	Osia777		Breed			12,50	2			905,25		2020-11-30
OstapBender	ostap@mail.com	Osia777		Meat			81,20	2			905,25		2020-11-30
OstapBender	ostap@mail.com	Osia777		Sandals			470,35	1			905,25		2020-11-30
OstapBender	ostap@mail.com	Osia777		Forks			82,50	3			905,25		2020-11-30

//Sorting the categories in descending order of the number of unique items in it.
SELECT `Categories`.`name` AS `category_name`, `T2`.`unique_items` 
FROM `Categories`
INNER JOIN (SELECT `category_id`, COUNT(`category_id`) AS `unique_items`
from `Products`
GROUP BY `category_id`) T2 
ON `Categories`.`id` = `T2`.`category_id`
ORDER BY `unique_items` DESC;

//Result:
category_name	unique_items
food			5
dishes			4
shoes			3
clothes			3